<?php

namespace App\Controller\Admin;

use App\Entity\Tags;
use App\Entity\Produits;
use App\Controller\Admin\TagsCrudController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;

class DashboardController extends AbstractDashboardController
{
    public function __construct(private AdminUrlGenerator $adminUrlGenerator)
    {
      
    }
   
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
      
        $url = $this->adminUrlGenerator->setController(TagsCrudController::class)->generateUrl();
        return $this->redirect($url);
        // Option 1. You can make your dashboard redirect to some common page of your backend
        //
       //  $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        // return $this->redirect($adminUrlGenerator->setController(OneOfYourCrudController::class)->generateUrl());

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        // return $this->render('some/path/my-dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Gestion Nozama');
    }

    public function configureMenuItems(): iterable
    {
        // yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        //
      yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::section('Tags');
        yield MenuItem::subMenu('Actions', 'fas fa-briefcase')
            ->setSubItems([
                MenuItem::linkToCrud('Show Tags', 'fas fa-eye', Tags::class),
                MenuItem::linkToCrud('Add Tag', 'fas fa-plus', Tags::class)
                    ->setAction(Crud::PAGE_NEW),
            ]);
            yield MenuItem::section('Prduits');
        yield MenuItem::subMenu('Actions', 'fas fa-briefcase')
            ->setSubItems([
                MenuItem::linkToCrud('Show Products', 'fas fa-eye', Produits::class),
                MenuItem::linkToCrud('Add Product', 'fas fa-plus', Produits::class)
                    ->setAction(Crud::PAGE_NEW),
            ]);
    
    }
}