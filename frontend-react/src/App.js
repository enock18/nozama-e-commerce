import { Route, Routes } from "react-router-dom";
import ScrollToTop from "./components/ScrollToTop/ScrollToTop";
import "./index.css";
import Apropos from "./pages/Apropos/Apropos";

import Home from "./pages/home/Home";
import NotFound from "./pages/NotFound/NotFound";

function App() {
  return (
    <>
      <ScrollToTop />
      <Routes>
        <Route index element={<Home />} />
        <Route path="contact" element={<Home />} />
        <Route path="/apropos" element={<Apropos />} />
        {/* <Route path="offers" element={<Offers />} />
        <Route path="offers/:offerId" element={<Offer />} />
        <Route path="/apply/message/:offerName" element= />
        <Route path="/contact/message" element={<ContactMessage />} />
        <Route path="apply/:offerTitle/:offerId" element={<ApplyPage />} />*/}
        <Route path="*" element={<NotFound />} />
      </Routes>
    </>
  );
}

export default App;
