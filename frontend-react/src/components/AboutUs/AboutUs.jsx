import React from "react";

function AboutUs() {
  return (
    <div className="h-80 mx-auto flex-grow flex items-center justify-center">
      <div className="flex flex-col items-center justify-center gap-2 p-5 md:w-1/2">
        <h2 className="text-3xl font-bold tracking-wide mb-5 md:text-4xl">
          About Us
        </h2>

        <p className="text-xl text-center font-normal ">
          Nozama en France : une contribution positive à l’économie Nous servons
          avec passion nos clients en France. Toutefois, si nous pensons d’abord
          à eux, nous accordons également la plus grande importance à notre
          empreinte économique, sociale et environnementale au niveau local. En
          mettant toute notre énergie, notre savoir-faire et notre capacité
          d’innovation au service des Français, nous contribuons à la croissance
          de l’économie française. Nous souhaitions partager un aperçu de la
          manière dont nous contribuons à la croissance de l’économie française,
          à la création de milliers d’emplois, au financement des services
          publics et du modèle social français ; tout en prenant soin de
          préserver l’environnement
        </p>
      </div>
    </div>
  );
}

export default AboutUs;
