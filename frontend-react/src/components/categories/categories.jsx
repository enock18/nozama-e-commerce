import React from "react";

function Categories(props) {
  return (
    <div className="categories text-white  basis-1/4">
      <h1 className="text-3xl">Filtrer par categories</h1>
      <hr className="text-3xl w-60" />
      <ul className="ml-5">
        <li>
          <a href="#">props.categories</a>
        </li>
        <li>
          <a href="#">props.categories</a>
          <a href="#" />
        </li>
        <li>
          <a href="#">props.categories</a>
          <a href="#" />
        </li>
        <li>
          <a href="#">props.categories</a>
        </li>
      </ul>
    </div>
  );
}

export default Categories;
