import React from "react";

function ButtonPanier(props) {
  return (
    <button
      className="btn-ajouter-panier w-full font-semibold text-md py-1 bg-yellow-button text-white"
      type="submit"
    >
      {props.name}
    </button>
  );
}

export default ButtonPanier;
