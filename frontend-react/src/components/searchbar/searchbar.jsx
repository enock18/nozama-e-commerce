import React from "react";
function Searchbar(props) {
  return (
    // <<!-- component -->
    // <!-- This is an example component -->
    <div className="pt-2 relative mx-auto text-gray-600">
      <div className="flex items-center justify-center">
        <div className="flex border-2 rounded">
          <input
            type="text"
            className="px-4 py-2 w-80"
            placeholder="Search..."
          />
          <button className="flex items-center justify-center px-4 border-l rounded-3">
            <svg
              className="w-6 h-6 text-gray-600"
              fill="currentColor"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 24 24"
            >
              <path d="M16.32 14.9l5.39 5.4a1 1 0 0 1-1.42 1.4l-5.38-5.38a8 8 0 1 1 1.41-1.41zM10 16a6 6 0 1 0 0-12 6 6 0 0 0 0 12z" />
            </svg>
          </button>
        </div>
      </div>
      <h2 className="titre-home text-white text-center text-3xl my-6">
        {props.title}
      </h2>
    </div>
  );
}

export default Searchbar;
