import React from "react";
import { Link } from "react-router-dom";
function NotFound() {
  return (
    <section className="page_404 container mx-auto">
      <div className="row">
        <div className="col-sm-12 ">
          <div className="col-sm-10 col-sm-offset-1  text-center">
            <h1 className="text-center code_404 mt-2">404</h1>
            <div className="four_zero_four_bg"></div>
            <div className="contant_box_404">
              <h3 className="text-3xl mb-1">Look like you're lost !</h3>

              <p>The page you are looking for doesn't exist</p>

              <Link to="/" className="link_404">
                Go to Homepage
              </Link>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default NotFound;
