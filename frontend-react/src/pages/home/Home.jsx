import React from "react";
import Layout from "../../components/layout/Layout";
import Searchbar from "../../components/searchbar/searchbar";
import Produit from "../../components/produit/produit";
// import LinkFooter from "../../components/LinkFooter/LinkFooter";
// import LastOffers from "../../components/LastOffers/LastOffers";
function Home() {
  return (
    <>
      <Layout>
        <Searchbar title="Nos Ventes Flash et Promotions" />
        <Produit />
      </Layout>
    </>
  );
}

export default Home;
