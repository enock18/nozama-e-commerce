import React from "react";
import Layout from "../../components/layout/Layout";
import Searchbar from "../../components/searchbar/searchbar";

function Apropos() {
  return (
    <>
      <Layout>
        <Searchbar title="A propos de nous" />
        <div className="text-white m-10">
          Nozama en France : une contribution positive à l’économie Nous servons
          avec passion nos clients en France. Toutefois, si nous pensons d’abord
          à eux, nous accordons également la plus grande importance à notre
          empreinte économique, sociale et environnementale au niveau local. En
          mettant toute notre énergie, notre savoir-faire et notre capacité
          d’innovation au service des Français, nous contribuons à la croissance
          de l’économie française. Nous souhaitions partager un aperçu de la
          manière dont nous contribuons à la croissance de l’économie française,
          à la création de milliers d’emplois, au financement des services
          publics et du modèle social français ; tout en prenant soin de
          préserver l’environnement
        </div>
      </Layout>
    </>
  );
}

export default Apropos;
