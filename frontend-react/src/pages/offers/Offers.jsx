import React from "react";
import Layout from "../../components/layout/Layout";
import PageLabel from "../../components/PageLabel/PageLabel";
import AllOffers from "../../components/AllOffers/AllOffers";

function Offers() {
  return (
    <>
      <Layout>
        <PageLabel label="Offers" />
        <AllOffers />
      </Layout>
    </>
  );
}

export default Offers;
