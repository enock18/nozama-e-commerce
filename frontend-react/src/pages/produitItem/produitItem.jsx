import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Layout from "../../components/layout/Layout";
import OfferWrap from "../../components/OfferWrap/OfferWrap";
import PageLabel from "../../components/PageLabel/PageLabel";

function produitItem() {
  const { produitId } = useParams();
  const [offer, setProduit] = useState({});
  const getOffer = async () => {
    const response = await axios(
      `${process.env.REACT_APP_LOCAL_HOST}/api/produits/${produitId}`
    );
    const data = response.data;
    setProduit(data);
  };

  useEffect(() => {
    getProduit();
  }, []);

  return <></>;
}

export default produitItem;
