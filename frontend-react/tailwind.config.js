const colors = require("tailwindcss/colors");

module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    colors: {
      "sky-blue": "#00A9FF",
      "yellow-button": "#F5953B",
      "date-card": "#666666",
      "gray-btn": "#CCCCCC",
      "red-price": "#DC1313",
      black: colors.black,
      white: colors.white,
      gray: colors.gray,
      emerald: colors.emerald,
      indigo: colors.indigo,
      yellow: colors.yellow,
      red: colors.red,
      amber: colors.amber,
      green: colors.green,
      purple: colors.purple,
      rose: colors.rose,
      stone: colors.stone,
    },
    extend: {},
  },
  plugins: [require("@tailwindcss/line-clamp")],
};
